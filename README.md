In this data assignment you will transform raw data of IPL to calculate the following stats:
1. Number of matches played per year for all the years in IPL.
2. Number of matches won of per team per year in IPL.
3. Extra runs conceded per team in 2016
4. Top 10 economical bowlers in 2015

Implement the 4 functions, one for each task.
Use the functions results to dump json files in the output folder

Directory structure:

- src/
  - server/
    - ipl.js
    - index.js
  - output/
    - matchesPerYear.json
  - data/
    - matches.csv
    - deliveries.csv
- package.json
- package-lock.json
- .gitignore


More questions in IPL project for practice:
- (Find the number of times each team won the toss and also won the match)
- (Find player per season who has won the highest number of *Player of the Match* awards)
- (Find the strike rate of the batsman Virat Kohli for each season)
- (Find the highest number of times one player has been dismissed by another player)
- (Find the bowler with the best economy in super overs)