const csvToJson = require('csvtojson');
const fs = require('fs');
exports.batsManStrikeRate = function (inputPath, inputPath2, outputPath, player) {
    csvToJson().fromFile(inputPath).then(
        source => {
            let matchesIdPerYear = {};
            let batsManData = {};
            source.forEach(element => {
                let year = element['season'];
                let id = element['id'];
                matchesIdPerYear[id] = year;
            });
            csvToJson({ checkType: true }).fromFile(inputPath2).then(
                source1 => {
                    source1.forEach(element1 => {
                        let batsman = element1['batsman'];

                        if (batsman === player) {
                            let id = element1['match_id'];
                            let wideball = element1['wide_runs'];
                            let noball = element1['noball_runs'];
                            let runs = element1['batsman_runs'];
                            let ball = 1;
                            if (matchesIdPerYear[id] in batsManData) {
                                if (noball > 0 || wideball > 0) {
                                    ball = 0;
                                }
                                batsManData[matchesIdPerYear[id]].balls += ball;
                                batsManData[matchesIdPerYear[id]].toatl_runs += runs;
                            } else {
                                if (noball > 0 || wideball > 0) {
                                    ball = 0;
                                }
                                batsManData[matchesIdPerYear[id]] = {
                                    balls: ball,
                                    toatl_runs: runs
                                };
                            }
                        }
                    });
                    let playerStrikeRateData = [];
                    for (let item in batsManData) {
                        let balls = batsManData[item].balls;
                        let runs = batsManData[item].toatl_runs;
                        let strikerate = parseFloat(((runs / balls) * 100).toFixed(2));
                        playerStrikeRateData.push([item, strikerate]);
                    }
                    fs.writeFile(outputPath, JSON.stringify(playerStrikeRateData), function (error) {
                        if (error) {
                            console.error("write error:  " + error.message);
                        } else {
                            console.log("Successful Write to " + outputPath);
                        }
                    });
                }
            );
        }
    );
}
