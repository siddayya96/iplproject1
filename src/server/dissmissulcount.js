const csvToJson = require('csvtojson');
const fs = require('fs');

exports.dismissalCount = function (inputPath, outputPath) {
    csvToJson().fromFile(inputPath).then(
        source1 => {
            let playerData = {};
            source1.forEach(element1 => {
                let player = element1['player_dismissed'];
                let reason = element1['dismissal_kind'];
                if (reason !== 'run out' && reason !== "") {
                    let bowler = element1['bowler'];
                    if (player in playerData) {
                        if (bowler in playerData[player]) {
                            playerData[player][bowler] += 1;
                        } else {
                            playerData[player][bowler] = 1;
                        }
                    } else {
                        playerData[player] = {};
                        playerData[player][bowler] = 1;
                    }
                }
            });
            let arrPlayerData = [];
            for (let batsman in playerData) {
                let max = 0;
                let topBowler = '';
                for (let bowler in playerData[batsman]) {
                    if (playerData[batsman][bowler] > max) {
                        max = playerData[batsman][bowler];
                        topBowler = bowler;
                    }
                }
                if (max > 4) {
                    arrPlayerData.push([batsman + '-' + topBowler, max]);
                }
            }
            fs.writeFile(outputPath, JSON.stringify(arrPlayerData), function (error) {
                if (error) {
                    console.error("write error:  " + error.message);
                } else {
                    console.log("Successful Write to " + outputPath);
                }
            });
        }
    );
}