const csvToJson = require('csvtojson');
const fs = require('fs');


exports.economicalBowler = function (inputPath, inputPath2, outputPath, yearGiven) {
    csvToJson({ checkType: true }).fromFile(inputPath).then(
        source => {
            let bowlerData = {};
            let ids = [];
            source.forEach(element => {
                let year = element['season'];
                if (year == yearGiven) {
                    ids.push(element['id']);
                }
            });
            csvToJson({ checkType: true }).fromFile(inputPath2).then(
                source1 => {
                    source1.forEach(element1 => {
                        let id = element1['match_id'];
                        if (ids.includes(id)) {
                            let bowler = element1['bowler'];
                            let ball = element1['wide_runs'] - element1['noball_runs'];
                            let byeRun = element1['bye_runs'];
                            let legByeRun = element1['legbye_runs'];
                            let run = element1['total_runs'] - legByeRun - byeRun;

                            if (bowler in bowlerData) {
                                bowlerData[bowler].runs += run;
                                if (ball == 0) {
                                    bowlerData[bowler].balls += 1;
                                }
                            } else {
                                bowlerData[bowler] = {
                                    balls: 1,
                                    runs: run
                                }
                            }
                        }
                    });
                    let bowlerEconomy = [];
                    for (let i in bowlerData) {
                        bowlerEconomy.push([i, parseFloat((bowlerData[i].runs / (bowlerData[i].balls / 6)).toFixed(2))]);
                    }
                    bowlerEconomy = bowlerEconomy.sort((i, j) => i[1] - j[1]);
                    bowlerEconomy = bowlerEconomy.slice(0, 10);
                    fs.writeFile(outputPath, JSON.stringify(bowlerEconomy), function (error) {
                        if (error) {
                            console.error("write error:  " + error.message);
                        } else {
                            console.log("Successful Write to " + outputPath);
                        }
                    });
                }
            );
        }
    );
}