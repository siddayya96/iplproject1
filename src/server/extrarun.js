const csvToJson = require('csvtojson');
const fs = require('fs');

exports.extraRunsPerTeam = function (inputPath, inputPath2, outputPath, onYear) {
    csvToJson().fromFile(inputPath).then(
        source => {
            let extrasPerTeam = {};
            let idds = [];
            source.forEach(element => {
                let year = element['season'];
                if (year == onYear) {
                    idds.push(element['id']);
                }
            });
            csvToJson().fromFile(inputPath2).then(
                source1 => {
                    source1.forEach(element1 => {
                        let team = element1['bowling_team'];
                        let extraRuns = parseInt(element1['extra_runs']);
                        let id = element1['match_id'];
                        if (idds.includes(id)) {
                            if (extrasPerTeam[team] > 0) {
                                extrasPerTeam[team] += extraRuns;
                            } else {
                                extrasPerTeam[team] = extraRuns;
                            }
                        }
                    });
                    let extraRuns = [];
                    for (let key in extrasPerTeam) {
                        extraRuns.push([key, extrasPerTeam[key]]);

                    }
                    fs.writeFile(outputPath, JSON.stringify(extraRuns), function (error) {
                        if (error) {
                            console.error("write error:  " + error.message);
                        } else {
                            console.log("Successful Write to " + outputPath);
                        }
                    });
                }
            );
        }
    );
}