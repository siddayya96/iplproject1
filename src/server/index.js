const testMatchesWon = require('./matcheswon');
const testMatchCountPerYear = require('./matchcountperyear');
const testEconomicalBowler = require('./economicalbowler');
const testExtraRuns = require('./extrarun');
const testTeamcountWonTossAndMatch = require('./wonTossAndMatch');
const testPlayerWithMaxCountOfPOTMAward = require('./playerswonpotm');
const testBatsmanStrikeRate = require('./batsmanstrikerate.js');
const testDissmisselCount = require('./dissmissulcount');
const testSuperOverEconomy = require('./superovereconomybowler');

testMatchesWon.matchesWon('./../data/matches.csv', "./../output/matchesWon.json")
testMatchCountPerYear.matchCountPerYear('./../data/matches.csv', "./../output/matchcountperyear.json");
testEconomicalBowler.economicalBowler('./../data/matches.csv', './../data/deliveries.csv', "./../output/bowlereconomy.json", "2015");
testExtraRuns.extraRunsPerTeam('./../data/matches.csv', './../data/deliveries.csv', "./../output/extrarunperteam.json", "2016");
testTeamcountWonTossAndMatch.wonBothTossAndMatchCount('./../data/matches.csv', "./../output/teamCountWonTossAndMatch.json");
testPlayerWithMaxCountOfPOTMAward.playerOfTheMatchCount('./../data/matches.csv', "./../output/playerofthematchaward.json");
testBatsmanStrikeRate.batsManStrikeRate('./../data/matches.csv', './../data/deliveries.csv', "./../output/playerStrikeRatePerYear.json", 'V Kohli');
testDissmisselCount.dismissalCount('./../data/deliveries.csv', './../output/dissmissulcount.json');
testSuperOverEconomy.superOverBowlerEconomy('./../data/deliveries.csv', './../output/superovereconomy.json');