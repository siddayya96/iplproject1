const csvToJson = require('csvtojson');
const fs = require('fs');

exports.matchCountPerYear = function (inputPath, outputPath) {
    csvToJson().fromFile(inputPath).then(
        source => {
            let matchesCountPerYear = {};
            matchesCountPerYear = source.reduce((prevData, curData) => {
                let year = curData['season'];
                (prevData[year] > 0) ? prevData[year] += 1 : prevData[year] = 1;
                return prevData;
            }, {});
            let arrMatchCount = [];
            for (let key in matchesCountPerYear) {
                arrMatchCount.push([key, matchesCountPerYear[key]]);

            }
            fs.writeFile(outputPath, JSON.stringify(arrMatchCount), function (error) {
                if (error) {
                    console.error("write error:  " + error.message);
                } else {
                    console.log("Successful Write to " + outputPath);
                }
            });
        }
    );
}
