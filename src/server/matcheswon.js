const csvToJson = require('csvtojson');
const fs = require('fs');

exports.matchesWon = function (inputPath, outputPath) {
    csvToJson({ checkType: true }).fromFile(inputPath).then(
        source => {
            let teamData = {};
            source.forEach(element => {
                let year = element['season'];
                let winner = element['winner'];
                if (teamData[winner] !== undefined) {
                    if (teamData[winner][year] > 0) {
                        teamData[winner][year] += 1;
                    } else {
                        teamData[winner][year] = 1;
                    }
                }
                else {
                    if (winner !== '') {
                        teamData[winner] = {};
                        teamData[winner][year] = 1;
                    }
                }
            });
            let arrTeamData = [];
            for (let item in teamData) {
                let arr = [0, 0, 0, 0, 0, 0, 0, 0, 0];
                for (let key in teamData[item]) {
                    arr[parseInt(key) - 2008] = teamData[item][key];
                }
                arrTeamData.push({
                    name: item,
                    data: arr
                });
            }
            fs.writeFile(outputPath, JSON.stringify(arrTeamData), function (error) {
                if (error) {
                    console.error("write error:  " + error.message);
                } else {
                    console.log("Successful Write to " + outputPath);
                }
            });
        }
    );
}