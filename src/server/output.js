const http = require('http')
const fs = require('fs')
const server = http.createServer((request, response) => {
    const path = request.url;
    if (path === '/') {
        fs.readFile('./index.html', 'UTF-8', (error, data) => {
            if (error) {
                switch (error.code) {
                    case 'ENOENT': {
                        response.writeHead(404, { 'Content-Type': 'text/html' });
                        response.write('File Not Found');
                        response.end();
                        break;
                    } default: {
                        response.writeHead(404, { 'Content-Type': 'text/html' });
                        response.write('Retry after some time.');
                        response.end();
                        break;
                    }
                }
            } else {
                response.writeHead(200, { 'Content-Type': 'text/html' });
                response.write(data);
                response.end();
            }
        });
    } else if (path.slice(path.length - 4, path.length) === 'json') {
        fs.readFile('./../output/' + path, 'UTF-8', (error, data) => {
            if (error) {
                switch (error.code) {
                    case 'ENOENT': {
                        response.writeHead(404, { 'Content-Type': 'text/json' });
                        response.write('File Not Found');
                        response.end();
                        break;
                    } default: {
                        response.writeHead(404, { 'Content-Type': 'text/json' });
                        response.write('Retry after some time.');
                        response.end();
                        break;
                    }
                }
            } else {
                response.writeHead(200, { 'Content-Type': 'text/json' });
                response.write(data);
                response.end();
            }
        });
    } else {
        fs.readFile('./' + path, 'UTF-8', (error, data) => {
            if (error) {
                switch (error.code) {
                    case 'ENOENT': {
                        response.writeHead(404, { 'Content-Type': 'text/html' });
                        response.write('File Not Found');
                        response.end();
                        break;
                    } default: {
                        response.writeHead(404, { 'Content-Type': 'text/html' });
                        response.write('Retry after some time.');
                        response.end();
                        break;
                    }
                }
            } else {
                response.writeHead(200, { 'Content-Type': 'text/html' });
                response.write(data);
                response.end();
            }
        });
    }
});
server.listen(8000);
