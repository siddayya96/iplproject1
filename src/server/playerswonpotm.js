const csvToJson = require('csvtojson');
const fs = require('fs');
exports.playerOfTheMatchCount = function (inputPath, outputPath) {
    csvToJson().fromFile(inputPath).then(
        source => {
            let season = {};
            let check = {};
            let prevYear = 0;

            source.forEach(element => {
                let year = element['season'];
                let playerOfTheMatch = element['player_of_match'];

                if (prevYear != year) {
                    let max = 0;
                    for (let item in check) {
                        if (check[item] >= max) {
                            if (check[item] >= max) {
                                if (check[item] == max) {
                                    season[prevYear][0] += ", " + item;
                                } else {
                                    max = check[item];
                                    season[prevYear] = [item, max];
                                }
                            }
                        }
                    }
                    check = {};
                    check[playerOfTheMatch] = 1;
                    prevYear = year;
                } else {
                    if (playerOfTheMatch in check) {
                        check[playerOfTheMatch] += 1;
                    } else {
                        check[playerOfTheMatch] = 1;
                    }
                }
            });
            let max = 0
            for (let item in check) {
                if (check[item] >= max) {
                    if (check[item] == max) {
                        season[prevYear][0] += item;
                    } else {
                        max = check[item];
                        season[prevYear] = [item, max];
                    }
                }
            }
            let playerWon = [];
            for (key in season) {
                playerWon.push([key + '-' + season[key][0], season[key][1]]);

            }
            fs.writeFile(outputPath, JSON.stringify(playerWon), function (error) {
                if (error) {
                    console.error("write error:  " + error.message);
                } else {
                    console.log("Successful Write to " + outputPath);
                }
            });
        }
    );
}
