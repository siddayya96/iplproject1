const csvToJson = require('csvtojson');
const fs = require('fs');
exports.superOverBowlerEconomy = function (inputPath, outputPath) {
    csvToJson().fromFile(inputPath).then(
        source1 => {
            let bowlerData = {};

            source1.forEach(element1 => {
                let superover = parseInt(element1['is_super_over']);
                if (superover === 1) {
                    let bowler = element1['bowler'];
                    let ball = element1['wide_runs'] - element1['noball_runs'];
                    let byeRun = element1['bye_runs'];
                    let legByeRun = element1['legbye_runs'];
                    let run = parseInt(element1['total_runs']) - legByeRun - byeRun;

                    if (bowler in bowlerData) {
                        bowlerData[bowler].runs += run;
                        if (ball == 0) {
                            bowlerData[bowler].balls += 1;
                        }
                    } else {
                        bowlerData[bowler] = {
                            balls: 1,
                            runs: run
                        };
                    }
                }
            });

            let bowlerEconomy = [];
            for (let i in bowlerData) {
                bowlerEconomy.push([i, parseFloat((bowlerData[i].runs / (bowlerData[i].balls / 6)).toFixed(2))]);
            }
            fs.writeFile(outputPath, JSON.stringify(bowlerEconomy), function (error) {
                if (error) {
                    console.error("write error:  " + error.message);
                } else {
                    console.log("Successful Write to " + outputPath);
                }
            });
        }
    );

}