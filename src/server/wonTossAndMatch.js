const csvToJson = require('csvtojson');
const fs = require('fs');

exports.wonBothTossAndMatchCount = function (inputPath, outputPath) {
    csvToJson().fromFile(inputPath).then(
        source => {
            let teamWinningCount = {};
            source.forEach(element => {
                let tossWinner = element['toss_winner'];
                let matchWinner = element['winner'];
                if (tossWinner === matchWinner) {
                    if (teamWinningCount[tossWinner] >= 0) {
                        teamWinningCount[tossWinner] += 1;
                    } else {
                        teamWinningCount[tossWinner] = 1;
                    }
                }
            });
            let teamWinning = [];
            for (let key in teamWinningCount) {
                teamWinning.push([key, teamWinningCount[key]]);
            }
            fs.writeFile(outputPath, JSON.stringify(teamWinning), function (error) {
                if (error) {
                    console.error("write error:  " + error.message);
                } else {
                    console.log("Successful Write to " + outputPath);
                }
            });
        }
    );
}